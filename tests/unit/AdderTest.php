<?php

use Codeception\Test\Unit;
use Mikk\MyAmazingProject\MathOperations;

class AdderTest extends Unit
{
    public function testAdder()
    {
        $mathOperatior = new MathOperations;

        $this->assertEquals(3, $mathOperatior->add(1,2));
    }
}
