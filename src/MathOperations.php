<?php

namespace Mikk\MyAmazingProject;

use Mikk\MyAmazingPackage\Adder;

class MathOperations
{
    public function add($a, $b)
    {
        return (new Adder)->add($a, $b);
    }
}
